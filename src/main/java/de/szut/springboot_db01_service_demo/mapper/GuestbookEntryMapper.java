package de.szut.springboot_db01_service_demo.mapper;

import de.szut.springboot_db01_service_demo.model.GuestbookEntry;
import de.szut.springboot_db01_service_demo.request.GuestbookEntryRequest;
import de.szut.springboot_db01_service_demo.response.GuestbookEntryResponse;
import org.springframework.stereotype.Service;

@Service
public class GuestbookEntryMapper {

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public GuestbookEntry mapRequestToModel(GuestbookEntryRequest request) {
        if (request == null) { return null; }
        GuestbookEntry model = new GuestbookEntry();
        model.setTitle(request.getTitle());
        model.setComment(request.getComment());
        model.setCommenter(request.getCommenter());
        return model;
    }

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public GuestbookEntry mapRequestToModel(long id, GuestbookEntryRequest request) {
        if (request == null) { return null; }
        GuestbookEntry model = mapRequestToModel(request);
        model.setId(id);
        return model;
    }

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public GuestbookEntryResponse mapModelToResponse(GuestbookEntry model) {
        if (model == null) { return null; }
        GuestbookEntryResponse response = new GuestbookEntryResponse();
        response.setId(model.getId());
        response.setTitle(model.getTitle());
        response.setComment(model.getComment());
        response.setCommenter(model.getCommenter());
        response.setDate(model.getDate());
        return response;
    }

}
